const { createClient } = require('@supabase/supabase-js');
const { SUPABASE_URL, SUPABASE_API_KEY } = require('../config.js');

const supabaseUrl = SUPABASE_URL;
const supabaseKey = SUPABASE_API_KEY;
const supabase = createClient(supabaseUrl, supabaseKey);
const maindataTable = 'maindata';

const getProfile = async (id) => {
  try {
    // Retrieve the profile by ID
    const { data: profile, error } = await supabase
      .from(maindataTable)
      .select('name, description, conditions, treatments, website, address, email, phone, id')
      .eq('id', id)
      .single();

    if (error) {
      console.error('Error retrieving profile:', error);
      return { error: 'Failed to retrieve profile' };
    }

    if (!profile) {
      return { error: 'Profile not found' };
    }

    // Return the profile data
    return profile;
  } catch (error) {
    console.error('Error retrieving profile:', error);
    return { error: 'Failed to retrieve profile' };
  }
};

module.exports = {
  getProfile
};
