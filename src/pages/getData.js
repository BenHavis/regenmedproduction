const { createClient } = require('@supabase/supabase-js');
const { SUPABASE_URL, SUPABASE_API_KEY, GOOGLE_MAPS_API_KEY } = require('../config.js');
const geolib = require('geolib');
const fetch = require('node-fetch');
const supabaseUrl = SUPABASE_URL;
const supabaseKey = SUPABASE_API_KEY;
const supabase = createClient(supabaseUrl, supabaseKey);
const maindataTable = 'maindata';

const geocodeCity = async (city, state, country) => {
  try {
    const address = `${city}, ${state}, ${country}`;
    const encodedAddress = encodeURIComponent(address);
    const apiKey = GOOGLE_MAPS_API_KEY;

    const response = await fetch(
      `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=${apiKey}`
    );

    const data = await response.json();

    const results = data.results;
    console.log('geocode results:', results);

    if (results && results.length > 0) {
      const result = results[0];
      const { lat, lng } = result.geometry.location;
      return { latitude: lat, longitude: lng };
    } else {
      console.log('No results found for the city:', city, state, country);
      return null;
    }
  } catch (error) {
    console.log('Error geocoding:', error);
    return null;
  }
};

const getData = async (filterTerm, checkboxOptions, city, state, country, maxDistance = 25) => {
  console.log('Executing getData...');
  console.log('Filter term from getData:', filterTerm);
  console.log('Checkbox options:', checkboxOptions);
  console.log('City:', city);
  console.log('State:', state);
  console.log('Country:', country);
  console.log('Max Distance:', maxDistance);

  try {
    let query = supabase.from(maindataTable).select('*').eq('conditions', filterTerm);

    if (checkboxOptions && checkboxOptions.length > 0) {
      const checkedLabels = checkboxOptions
        .filter(option => option.checked)
        .map(option => option.label);

      if (checkedLabels.length > 0) {
        query = query.or(`treatment ilike any (${checkedLabels.map(label => `%${label}%`).join(', ')})`);
      }
    }

    const cityLocation = await geocodeCity(city, state, country);
    if (!cityLocation) {
      console.log('Invalid location:', city, state, country);
      return { error: 'Invalid location' };
    }

    const cityLatitude = cityLocation.latitude;
    const cityLongitude = cityLocation.longitude;

    const { data: allData, error } = await query;
    if (error) {
      console.error('Error executing query:', error);
      return { error: 'Internal Server Error' };
    }

    console.log('All Rows:', allData);
   
    const filteredData = allData.filter(item => {
        const latitude = parseFloat(item.latitude);
        const longitude = parseFloat(item.longitude);
      
        if (!isNaN(latitude) && !isNaN(longitude)) {
          const distance = geolib.getDistance(
            { latitude, longitude },
            { latitude: parseFloat(cityLatitude), longitude: parseFloat(cityLongitude) }
          );
          const distanceInKm = distance / 1000; // Convert distance from meters to kilometers
          return distanceInKm <= maxDistance;
        }
        return false;
      });
      
    console.log('Filtered Rows:', filteredData);

    return { data: filteredData };
  } catch (error) {
    console.error('Error executing query:', error);
    return { error: 'Internal Server Error' };
  }
};

module.exports = {
  getData
};
